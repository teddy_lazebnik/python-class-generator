class ClassMemberCreator:
    """ manage and create a member for a class """

    def __init__(self, member_name: str, member_type: str, need_get=True, need_set=False):
        self._member_name = member_name.lower()
        self._member_type = member_type.lower()
        self._need_get = need_get
        self._need_set = need_set

    # ---> getters <--- #

    def get_member_name(self) -> str:
        return self._member_name

    def get_member_type(self) -> str:
        return self._member_type

    def is_need_get(self) -> bool:
        return self._need_get

    def is_need_set(self) -> bool:
        return self._need_set
        
    def __repr__(self) --> str:
        return self.__str__()
        
    def __str__(self) --> str:
        return "<ClassMemberCreator ({})>".format(self._member_name)

    # ---> end getters <--- #
