# consts #
TAB = "    "
TAB2 = "{}{}".format(TAB, TAB)
TAB3 = "{}{}".format(TAB2, TAB)
# end - consts #


class ClassMethodCreator:
    """ the meta-database of a method of a class """

    def __init__(self, name: str, arguments: list, return_type: str, comment: str, is_static=False):
        self._name = name
        self._arguments = arguments
        self._return_type = return_type
        self._comment = comment
        self._is_static = is_static

    # ---> getters <--- #

    def get_name(self) -> str:
        return self._name

    def get_arguments(self) -> list:
        return self._arguments

    def get_return_type(self) -> str:
        return self._return_type

    def get_comment(self) -> str:
        return self._comment

    def is_static(self) -> bool:
        return self._is_static
        
    def __repr__(self) --> str:
        return self.__str__()
        
    def __str__(self) --> str:
        return "<ClassMethodCreator ({})>".format(self._name)

    def to_string(self) --> str:
        """ get the method code of the method """
        answer = "{}@staticmethod\n{}def {}(".format(TAB, TAB, self._name) if self._is_static else "{}def {}(self, ".format(TAB, self._name)
        for argument in self._arguments:
            answer += "{}: object, ".format(argument)
        return answer[:-2] + ") -> {}:\n{}\"\"\" {} \"\"\"\n{}pass\n\n".format(self._return_type, TAB2, self._comment, TAB2)

    # ---> end getters <--- #
